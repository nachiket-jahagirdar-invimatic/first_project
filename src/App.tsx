import './App.css'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Index from './components/Index';
import Form1 from './components/Form1';
import Index2 from './components/Index2';
import Index3 from './components/Index3';

const App = () => {
  const router = createBrowserRouter([
  {
    path: "/",
    element: <Index />,
  },
  {
    path: "/Form1",
    element: <Form1 />,
  },
  {
    path: "/Form1/:id",
    element: <Form1 />,
  },
  {
    path: "/Index2",
    element: <Index2 />,
  },
  {
    path: "/Index3",
    element: <Index3 />,
  },
]);

  return (
     <RouterProvider router={router} />
  )
}

export default App
