import { Button, TextField } from "@mui/material";
import { Field, FormikProvider, useFormik } from "formik";
import { initials } from "./statics";
import { addEmployee, getEmployeeDataByIdApi, updateEmployee } from "./api";
import * as Yup from "yup"; 
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";

const Form1 = () => {
    const params= useParams();
    let navigate =useNavigate();
    const userSchema = Yup.object().shape({
        first_name:Yup.string()
        .required("please enter first name")
        .min(4,"minimum 4 char required")
        .matches(/[A-Z]/),
        last_name:Yup.string()
        .required("please enter last name")
        .min(4,"minimum 4 char required")
        .matches(/[A-Z]/),
        email:Yup.string()
        .required("please enter email")
        .email(),
        mob_no:Yup.number()
        .required("please enter first name")
        .min(1000000000)
        .max(9999999999)
        

    })
    const formik = useFormik({
        initialValues: initials,
        validationSchema:userSchema,
        onSubmit: (values) => {
            if(params.id){
                updateEmployee(formik.values)
                navigate('/')
            }else{
                addEmployee(formik.values)
                navigate('/')
            }
        }
    });
    const getEmployeeDataById =async()=>{
        try {
            const result:any = await getEmployeeDataByIdApi(params.id)
            formik.setValues(result.data)
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        if(params.id){
            getEmployeeDataById();
        }
      },[params.id]);
    return (<>Add Employee
        <br/>
        <FormikProvider value={formik}>
        <Field name="first_name">
                {({ field, meta, form }: any) => (<>
                    <TextField label="first_name"
                        onChange={formik.handleChange} value={formik.values}
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

        </Field>
            <br/>
            <br/>
            <Field name="last_name">
                {({ field, meta, form }: any) => (<>
                    <TextField label="last_name"
                        onChange={formik.handleChange} value={formik.values}
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <br/>
            <br/>
            <Field name="email">
                {({ field, meta, form }: any) => (<>
                    <TextField label="email"
                        onChange={formik.handleChange} value={formik.values}
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <br/>
            <br/>
            <Field name="mob_no">
                {({ field, meta, form }: any) => (<>
                    <TextField label="mob_no"
                        onChange={formik.handleChange} value={formik.values}
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <br/>
            <br/>
            <Button variant="outlined" onClick={()=>formik.handleSubmit}>submit</Button>
            {/* <Button onClick={()=>formik.handleSubmit()}> submit</Button> */}

        </FormikProvider>
    </>)
}
export default Form1;