import axios from "axios"
import { baseurl, empurl } from "./urls"


export const getEmployeeData = async() =>{
    try {
        return await axios.get(`${baseurl+empurl}`)
    } catch (error) {
        console.log(error)
    }
}

export const getEmployeeDataByIdApi = async(id:any) =>{
    try {
        return await axios.get(`${baseurl + empurl}/${id}`)
    } catch (error) {
        console.log(error)
    }
}

export const addEmployee = async(values:any) =>{
    try {
        return await axios.post(`${baseurl+empurl}`,values)
    } catch (error) {
        console.log(error)
    }
}

export const deleteEmployee = async(id:any) =>{
    try {
        return await axios.delete(`${baseurl+empurl}/${id}`)
    } catch (error) {
        console.log(error)
    }
}

export const updateEmployee = async(item:any) =>{
    try {
        return await axios.put(`${baseurl+empurl}/${item.id}`,item)
    } catch (error) {
        console.log(error)
    }
}

export const paginateData = async(payload:any) =>{
    try {
        return await axios.get(`${baseurl+empurl}`, {
            params: {
                ...(payload.page ? { _page: payload.page } : {}),
                ...(payload.limit ? { _limit: payload.limit } : {}),
                ...(payload.sort && payload.order ? { _sort: payload.sort } : {}),
                ...(payload.order ? { _order: payload.order } : {})
            }
        })
    } catch (error) {
        console.log(error)
    }
}

