import { Button, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@mui/material";
import { Field, FieldArray, Formik, FormikProvider, useFormik } from "formik";
import { initials } from "./statics";
import { useState } from "react";

const Index3 = () => {

    const formikTable = useFormik({
        initialValues: {
            table: [] as Array<any>
        } as any,
        onSubmit: (values) => {
        }
    });

    const formik = useFormik({
        initialValues: initials,
        onSubmit: (values) => {
            formikTable.setValues([...formikTable.values.table, formik.values])
        }
    });
    return (<>
        <FormikProvider value={formik}>
            <Field name="id">
                {({ field, meta, form }: any) => (<>
                    <TextField label="id"
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <Field name="first_name">
                {({ field, meta, form }: any) => (<>
                    <TextField label="first_name"
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <Field name="last_name">
                {({ field, meta, form }: any) => (<>
                    <TextField label="last_name"
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <Field name="email">
                {({ field, meta, form }: any) => (<>
                    <TextField label="email"
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <Field name="mob_no">
                {({ field, meta, form }: any) => (<>
                    <TextField label="mob_no"
                        {...field} />
                    {meta.touched && meta.error && (
                        <div >
                            {meta.error}
                        </div>
                    )}
                </>)}

            </Field>
            <Button variant="outlined" onClick={() => formik.handleSubmit}>submit</Button>
        </FormikProvider>



        <Table>
            <TableHead>

                <TableCell>first_name</TableCell>
                <TableCell>last_name</TableCell>
                <TableCell>email</TableCell>
                <TableCell>mob_no</TableCell>


            </TableHead>
            <TableBody>
                <FormikProvider value={formikTable}>
                    <FieldArray name="table" render={(formikHelper) => (<>

                    <button onClick={() => {
                        formikHelper.push({id: "1", first_name: "fwfwqf", last_name: "ascasc",
                        email: "ascasv",
                        mob_no: "casc awj "  
                    })
                    }}>Add</button>
                        {formikTable.values.table.map((item: any, index: number) => (<>
                            <TableRow>
                                <TableCell>{item.id}</TableCell>
                                <TableCell>{item.first_name}</TableCell>
                                <TableCell>{item.last_name}</TableCell>

                                <TableCell>{item.email}</TableCell>

                                <TableCell>{item.mob_no}</TableCell>
                                <TableCell>
                                    <button onClick={() => {
formikHelper.remove(index)
                                    }}>Delete</button>
                                </TableCell>


                            </TableRow>
                        </>))}

                    </>)} />
                </FormikProvider>



            </TableBody>
        </Table>
    </>)
}
export default Index3;