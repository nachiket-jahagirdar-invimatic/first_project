import { Button, FormControl, InputLabel, MenuItem, Select, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { deleteEmployee, getEmployeeData, paginateData } from "./api";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { initials } from "./statics";


interface Istate {
    payload: any,
    selectedRow: any
}
const Index = () => {
    let navigate = useNavigate();

    const [employeeList, setEmployeeList] = useState<any>([]);
    const [initialState, setInitialState] = useState<Istate>({
        selectedRow: null,
        payload: {
            page: 0,
            limit: 5,
            sort: '',
            order: ''
        }
    });
    const getData = async () => {
        const res: any = await paginateData(initialState.payload);
        setEmployeeList(res.data)
    }
    useEffect(() => {
        getData()
    }, []);
    const handledelete = (id: any) => {
        deleteEmployee(id)
    }
    const handleUpdate = (id: any) => {
        navigate(`/Form1/${id}`)
    }
    const handlePrevious = () => {
        if (initialState.payload.page != 0) {
            setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, page: prev.payload.page - 1 } }));
        } else {
            setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, page: 0 } }));
        }

    }
    const handleNext = () => {
        setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, page: prev.payload.page + 1 } }));
    }
    const handleLimit = (limit: any) => {
        setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, limit: limit } }));
    }
    const handleSort = async (sort: any) => {
        if (initialState.payload.order == '') {
            initialState.payload.order = 'asc'
        } else if (initialState.payload.order == 'asc') {
            initialState.payload.order = 'desc'
        } else {
            initialState.payload.order = ''
        }
        setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, sort: sort } }));
        // getData();
    }
    const formik = useFormik({
        initialValues: initials,

        onSubmit: (values) => {

        }
    });
    const handleEdit = (item: any) => {
        setInitialState((prev: Istate) => ({ ...prev, selectedRow: item }));
    }


    useEffect(() => {
        getData();
    }, [initialState.payload]);

    return (<>In the Index Page
        <br />
        <Button onClick={() => navigate('/Form1')}> Add Employee</Button>
        <Table>
            <TableHead>
                <TableCell><Button onClick={() => handleSort("id")}>id</Button></TableCell>
                <TableCell><Button onClick={() => handleSort("first_name")}>first_name</Button></TableCell>
                <TableCell>last_name</TableCell>
                <TableCell>email</TableCell>
                <TableCell>mob_no</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
            </TableHead>
            <TableBody>
                {employeeList.map((item: any) => {
                    return <TableRow>
                        <TableCell>{item.id}</TableCell>
                        <TableCell>{item.first_name}</TableCell>
                        <TableCell>{item.last_name}</TableCell>

                        <TableCell>{item.email}</TableCell>

                        <TableCell>{item.mob_no}</TableCell>
                        
                        <TableCell><Button onClick={() => handleUpdate(item.id)}>update</Button></TableCell>
                        <TableCell><Button onClick={() => handledelete(item.id)}>delete</Button></TableCell>
                    </TableRow>
                })}
            </TableBody>
        </Table>
        <Button onClick={() => handlePrevious()}>previous</Button>
        <Button onClick={() => handleNext()}>next</Button>
        <FormControl >
            <InputLabel>limit</InputLabel>
            <Select
                defaultValue={initialState.payload.limit}
                onChange={(event: any) => handleLimit(event.target.value)}
            >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={15}>15</MenuItem>
            </Select>
        </FormControl>
    </>)
}
export default Index;