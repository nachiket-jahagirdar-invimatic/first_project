import { Button, FormControl, InputLabel, MenuItem, Select, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { deleteEmployee, getEmployeeData, paginateData, updateEmployee } from "./api";
import { useNavigate } from "react-router-dom";
import { Field, FormikProvider, useFormik } from "formik";
import { initials } from "./statics";


interface Istate {
    payload: any
}
interface IFormikInitialState {
    selectedRow: any | null
}
const Index2 = () => {
    let navigate = useNavigate();

    const [employeeList, setEmployeeList] = useState<any>([]);
    const [initialState, setInitialState] = useState<Istate>({
        payload: {
            page: 0,
            limit: 5,
            sort: '',
            order: ''
        }
    });
    const getData = async () => {
        const res: any = await paginateData(initialState.payload);
        setEmployeeList(res.data)
    }
    useEffect(() => {
        getData()
    }, []);
    const handledelete = (id: any) => {
        deleteEmployee(id)
    }
    // const handleUpdate = (id: any) => {
    //     navigate(`/Form1/${id}`)
    // }
    const handlePrevious = () => {
        if (initialState.payload.page != 0) {
            setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, page: prev.payload.page - 1 } }));
        } else {
            setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, page: 0 } }));
        }

    }
    const handleNext = () => {
        setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, page: prev.payload.page + 1 } }));
    }
    const handleLimit = (limit: any) => {
        setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, limit: limit } }));
    }
    const handleSort = async (sort: any) => {
        if (initialState.payload.order == '') {
            initialState.payload.order = 'asc'
        } else if (initialState.payload.order == 'asc') {
            initialState.payload.order = 'desc'
        } else {
            initialState.payload.order = ''
        }
        setInitialState((prev: Istate) => ({ ...prev, payload: { ...prev.payload, sort: sort } }));
        // getData();
    }
    const formikInitialState: IFormikInitialState = {
        selectedRow: null
    };
    const formik = useFormik({
        initialValues: formikInitialState,
        onSubmit: (values) => {
            // updateEmployee(formik.values)
        }
    });
    const handleEdit = (item: any) => {
        formik.setFieldValue("selectedRow", item)
    }
    const handleUpdate = async () => {
        try {
            console.log(formik.values.selectedRow)
            const res = await updateEmployee(formik.values.selectedRow);
            if (res?.status === 200) {
                formik.setFieldValue("selectedRow", null);
                getData();
            }
            
        } catch (error) {
            console.log(error)
        }

    }

    useEffect(() => {
        getData();
    }, [initialState.payload]);

    return (<>In the Index Page
        <br />
        <Button variant="outlined" onClick={() => navigate('/Form1')}>Add Employee</Button>
        <Table>
            <TableHead>
                <TableCell><Button onClick={() => handleSort("id")}>id</Button></TableCell>
                <TableCell><Button onClick={() => handleSort("first_name")}>first_name</Button></TableCell>
                <TableCell>last_name</TableCell>
                <TableCell>email</TableCell>
                <TableCell>mob_no</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
            </TableHead>
            <TableBody>
                <FormikProvider value={formik}>
                    {employeeList.map((item: any) => {
                        return <TableRow>
                            <TableCell>{item.id}</TableCell>
                            <TableCell>
                                {formik.values.selectedRow?.id === item.id ? (<>
                                    <Field name="selectedRow.first_name">
                                        {({ field, meta, form }: any) => (<>
                                            <TextField label="first_name" {...field} />
                                            {meta.touched && meta.error && (
                                                <div >
                                                    {meta.error}
                                                </div>
                                            )}
                                        </>)}

                                    </Field>
                                </>) : (<>
                                    {item.first_name}
                                </>)}
                            </TableCell>

                            <TableCell>
                                {formik.values.selectedRow?.id === item.id ? (<>
                                    <Field name="selectedRow.last_name">
                                        {({ field, meta, form }: any) => (<>
                                            <TextField label="last_name"
                                                {...field} />
                                            {meta.touched && meta.error && (
                                                <div >
                                                    {meta.error}
                                                </div>
                                            )}
                                        </>)}

                                    </Field>
                                </>) : (<>
                                    {item.last_name}
                                </>)}
                            </TableCell>

                            <TableCell>{formik.values.selectedRow?.id === item.id ? (<>
                                <Field name="email">
                                    {({ field, meta, form }: any) => (<>
                                        <TextField label="email"
                                            onChange={formik.handleChange} value={formik.values}
                                            {...field} />
                                        {meta.touched && meta.error && (
                                            <div >
                                                {meta.error}
                                            </div>
                                        )}
                                    </>)}

                                </Field>
                            </>) : (<>
                                {item.email}
                            </>)}
                            </TableCell>

                            <TableCell>{formik.values.selectedRow?.id === item.id ? (<>
                                <Field name="mob_no">
                                    {({ field, meta, form }: any) => (<>
                                        <TextField label="mob_no"
                                            onChange={formik.handleChange} value={formik.values}
                                            {...field} />
                                        {meta.touched && meta.error && (
                                            <div >
                                                {meta.error}
                                            </div>
                                        )}
                                    </>)}

                                </Field>
                            </>) : (<>
                                {item.mob_no}
                            </>)}
                            </TableCell>
                            <TableCell>
                                {formik.values.selectedRow ? (<>
                                    <Button onClick={() => handleUpdate()}>update</Button>
                                </>) : (<>
                                    <Button onClick={() => handleEdit(item)}>edit</Button>
                                </>)}
                            </TableCell>
                            <TableCell><Button onClick={() => handledelete(item.id)}>delete</Button></TableCell>
                        </TableRow>
                    })}
                </FormikProvider>
            </TableBody>
        </Table>
        <Button onClick={() => handlePrevious()}>previous</Button>
        <Button onClick={() => handleNext()}>next</Button>
        <FormControl >
            <InputLabel>limit</InputLabel>
            <Select
                defaultValue={initialState.payload.limit}
                onChange={(event: any) => handleLimit(event.target.value)}
            >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={15}>15</MenuItem>
            </Select>
        </FormControl>
    </>)
}
export default Index2;